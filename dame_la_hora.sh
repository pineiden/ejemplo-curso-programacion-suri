#! /bin/bash

echo "Hola! Este programa te entrega la hora según el formato que le des:"
echo "------------------------------------------------------------------"
echo ""
read -p "Dame un formato y te doy la hora :>" FORMATO
while [ -n "$FORMATO" ];do
    echo "El formato es "$FORMATO
    hora=$(date +"${FORMATO}")
    echo "La hora es "$hora 
    read -p "Dame un formato y te doy la hora :>" FORMATO
done
